drop PROCEDURE if exists CheckSchedule;

DELIMITER $$

create PROCEDURE CheckSchedule
(in SchID varchar(10),in MvID varchar(10),out CinID varchar(10),out TimeSch DateTime)
BEGIN

	select CinemaID , TimeSchedule
	
	into CinID , TimeSch

	from MovieSchedules
	where
		ScheduleID = SchID and
		MovieID = MvID;

END $$
DELIMITER ;
