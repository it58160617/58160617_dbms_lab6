create table Register
(
	RegistID	varchar(10)	not null,
	ScheduleID	varchar(10)	not null,
	CustomerID	varchar(10)	not null,
	PRIMARY KEY(RegistID),
	FOREIGN KEY(ScheduleID) REFERENCES MovieSchedules(ScheduleID),
	FOREIGN KEY(CustomerID) REFERENCES Customers(CustomerID)
) Engine=InnoDB;
