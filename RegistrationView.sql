create view Registration as
	select MS.ScheduleID,MS.ScheduleName,M.MovieName,C.CinemaName,MS.TimeSchedule 
	
	from MovieSchedules as MS
	
	LEFT JOIN (Movies as M, Cinema as C)
	
	on MS.MovieID=M.MovieID and MS.CinemaID=C.CinemaID;
