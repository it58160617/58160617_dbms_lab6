create table MovieSchedules
(
	ScheduleID	varchar(10)	NOT NULL,
	ScheduleName	varchar(50)	NOT NULL,
	MovieID		varchar(10)	NOT NULL,
	CinemaID	varchar(10)	NOT NULL,
	TimeSchedule	DateTime	NOT NULL,
	PRIMARY KEY (ScheduleID),
	FOREIGN KEY(MovieID) REFERENCES Movies(MovieID),
	FOREIGN KEY(CinemaID) REFERENCES Cinema(CinemaID)
) Engine=InnoDB;
