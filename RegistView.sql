create view Regist as 
	
	select R.RegistID , MS.ScheduleID , MS.ScheduleName,C.CustomerName,C.CustomerSurname
	
	from Register as R
	JOIN (MovieSchedules as MS, Customers as C)

	on R.ScheduleID = MS.ScheduleID and R.CustomerID = C.CustomerID;
